<?php
 session_start();
 include 'headerLogged.php';
?>


<section class=" equal" style="">
    <section class="equal">
        <div class="container registeration">
        <?php
        require_once 'database/dbConfig.php';

// To get the Account details
            $profileQuery = "SELECT * FROM users WHERE email='".$_SESSION["email"]."'";
            $result = $Conn->query($profileQuery);
                                            
            if ($result->num_rows > 0){
                $rows = $result->fetch_assoc();
				echo'
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group"> 

                    <form class="form" method="post" id="registrationForm">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    Your Name*
                                </div>
                            </div>
                      
                            <div class="col-md-2">
                                <div class="form-group">                         
                                    <select name="title" id="title" class="form-control" required>
                                        <option value="">--</option>
                                        <option value="Ms." ';if($rows["title"]=="Ms."){echo 'selected="selected"';} echo'>Ms.</option>
                                        <option value="Mr." ';if($rows["title"]=="Mr."){echo 'selected="selected"';} echo'>Mr.</option>
                                        <option value="Mrs." ';if($rows["title"]=="Mrs."){echo 'selected="selected"';} echo'>Mrs.</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">                         
                                    <input type="text" id="name" class="capitalize form-control" required name="name" value="'.$rows["name"].'">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    US Phone Number*
                                </div>
                            </div>
                      
                            <div class="col-md-8">
                                <div class="form-group">                         
                                    <input type="text" id="us_phone" class="form-control" required name="us_phone" value="'.$rows["us_phone"].'">
                                </div>
                            </div>
                        </div>
						
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    Your Email
                                </div>
                            </div>
                      
                            <div class="col-md-8">
                                <div class="form-group">                         
                                    <input type="text" id="email" class="form-control" disabled="disabled" required name="email" value="'.$rows["email"].'">
                                </div>
                            </div>
                        </div>';
            }
    ?>

                        <h4>Change Password</h4>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    Old Password
                                </div>
                            </div>
                      
                            <div class="col-md-8">
                                <div class="form-group">                         
                                    <input type="text" id="opassword" class="form-control" name="opassword" value="">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    New Password
                                </div>
                            </div>
                      
                            <div class="col-md-8">
                                <div class="form-group">                         
                                    <input type="text" id="npassword" class="form-control" name="npassword" value="">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    Confirm Password
                                </div>
                            </div>
                      
                            <div class="col-md-8">
                                <div class="form-group">                         
                                    <input type="text" id="cpassword" class="form-control" name="cpassword" value="">
                                </div>
                            </div>
                        </div>

                        <input type="submit" name="accountUpdate" class="add btn btn-warning" value="Submit" />
                    </form>
                  </div>
                </div>
              </div>
          </div>
    </section>
</section>
 
<?php

include 'footer.php';

// To update Account details
if(isset($_POST["accountUpdate"])){
    if (!empty($_POST['npassword'])){
        if($_POST['opassword']==$rows['password']){
            if ($_POST['npassword'] == $_POST['cpassword']){
                $updateQuery = "UPDATE users SET title='".$_POST["title"]."', name='".$_POST["name"]."', us_phone='".$_POST["us_phone"]."', password='".$_POST["npassword"]."' WHERE email='".$_SESSION['email']."'";
                $Conn->query($updateQuery);
                if ($Conn->query($updateQuery) === TRUE) {
                    echo "<script>alert('Updated Successfully')</script>";
                }else{
                    echo "<script>alert('Some Problem Occured')</script>";
                }
            }else{
                echo'<script>alert("New Password & Confirm Password Do Not Match")</script>';
            } 
        }else{
            echo'<script>alert("Old Password is Incorrect")</script>';
        }
        
    }else{
        $updateQuery = "UPDATE users SET title='".$_POST["title"]."', name='".$_POST["name"]."', us_phone='".$_POST["us_phone"]."' WHERE email='".$_SESSION['email']."'";
        $Conn->query($updateQuery);
        if ($Conn->query($updateQuery) === TRUE) {
            echo "<script>alert('Updated Successfully')</script>";
        }else{
            echo "<script>alert('Some Problem Occured')</script>";
        }
    }
}
    


?>
<script>
    $("#us_phone").inputmask({"mask": "(999) 999-9999"});
</script>