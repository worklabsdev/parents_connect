<footer class="ftr-element">
<div class="container">
        <div class="row">
            <div class="col-md-5">
                <h4 class="footer-heading mt50">About Us</h4>
                <div class="about-footer mt10">
                    <p>
                        <span id="footer6_spnAbout"></span>
                       

                        <b>
                             India’s First and Only Travel, Portal for age 50 and Above<br><br>
<b>There’s a whole world left for us to explore</b><br><br>

Parents Holiday is targeted towards parents who have worked hard their whole life and have raised their children selflessly. We have designed custom tours for people above 50 years, in which .... <a href="about_us.php"> read more </a></b>
                    </p>
                    
                </div>
            </div>
            <div class="col-md-7">
                <div class="row">
                    <div class="col-md-4">
                        <h4 class="footer-heading mt50">Useful Links</h4>
                        <div class="about-footer mt10">
                            <ul>
                                <li><a href="about_us.php">About</a></li>
                                <li><a href="http://booking.parentsholiday.com/contact.aspx">Contact Us</a></li>
                                <li><a href="http://booking.parentsholiday.com/enquiry.aspx">Enquiry</a></li>
                                <li><a href="http://booking.parentsholiday.com/privacy-policy.aspx">Privacy Policy</a></li>
                                <li><a href="http://booking.parentsholiday.com/terms-and-conditions.aspx">Terms &amp; Conditions</a></li>

                            </ul>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <h4 class="footer-heading mt50">Contact Us</h4>
                        <div class="about-footer mt10">
                            <p>
                            Ground Floor, Building 8A, Cyber Hub, DLF Cyber City, <br>Gurgaon : 122010,<br>Haryana,<br>India
                                <br>
                                info@parentsholiday.com
                                <br>
                                <span id="spfootcont"><a href="tel:+1 470-287-0655"> +1 470-287-0655</a>,<a href="tel:+917291010700"> (+91)72910-10700</a></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>


<script src="js/jquery-2.2.4.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/slick.js"></script>
<script src="js/jquery.easing.js"></script>
<script src="js/jquery.kompleter.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/jquery.mb.YTPlayer.js"></script>
<script src="js/typeit.min.js"></script>
<script src="js/jquery.sticky.js"></script>
<script src="js/custom.js"></script>
<script src="js/jquery.inputmask.bundle.js"></script>
