<?php

//Included Header
include 'headerLogged.php';
?>   
<link href="css/frontend/style.css" rel="stylesheet" type="text/css"/>        
<section class="container-fluid add-profile"><!--get-connected-->
        <div class="row">
            <div class="col-md-12 width767 width1600 padding0 best-search-result">
                <div class="full-width margin-top50">
                    <div class="top-links">
                        <ul class="full-width">
                            <li>
                                <a href="best_match.php" title="Best Matches">Best Matches</a>
                            </li>
                            <li class="active-search">
                                <a href="advanced_search.php" title="Advanced Search">Advanced Search</a>                            
                            </li>
                        </ul>
					    <div class="search-results scroll-area">
                            <div id="pro-near">							
                                <div class="search-area full-width">
                                                                        	
                                      
                                    <form class="full-width form-horizontal" action="advanced_search.php">
                                        <div class="form-fields width100">
                                            <div class="full-width custom-form">
                                                <label><strong>Where would you like to find connections?</strong></label>
                                                <div class="col-lg-12 basic_search text-center">
                                                    <div class="form-group  has-feedback"> 
                                                        <input type="text" name="search" value="" id="search_zip_city_new" required min="5" max="5" placeholder="Enter City or Zip Code" class="form-control " autocomplete="new-password">
                                         
                                                    </div> 
                                                </div>
                                                
                                            </div>	   
                                        </div>&nbsp;
                                        <div class="button-section-new">
                                            <button class="btn btn-default button-sxn button-section-new" title="SEARCH" type="submit" name="searchWrap" id="search">Next</button>
                                        </div>
                                    </form>
                                </div>

                            </div>					
                        </div>
                        <div id="mapfeeds">
                                                           
                                <div class="search-results second-child">
                                    <div class="map-sxn">
                                        <div class="map-section" id="obj1">  
                                            
                                            <a class="next-step next-step-v7 js-next-step" href="#obj1"></a>
                                            <!-- <script src="https://www.visitorsconnect.com/js/frontend/gmaps.js"></script>  -->
                                            <!-- <style>
                                                #google-map { height: 700px !important; }
                                            </style> -->

                                            <div class="google-map-wrap" itemscope itemprop="hasMap" itemtype="http://schema.org/Map">
                                                <div id="google-map" class="google-map map">
                                                </div><!-- #google-map -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<!-- Modal -->

<?php include 'footer.php'; ?>

