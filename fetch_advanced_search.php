<?php
   session_start();
   require_once 'database/dbConfig.php';

   if(isset($_POST["action"]))
    {   

        // To get list of profiles already connected or requested for connection
        $connArray=array();
        $connections="SELECT * FROM connections WHERE user_id='".$_SESSION["primary_id"]."' OR connection_id='".$_SESSION["primary_id"]."'";
        
        $res = $Conn->query($connections);                              
            if ($res->num_rows > 0){
                while($row = $res->fetch_assoc()){
                    array_push($connArray,$row["connection_id"]);
                    array_push($connArray,$row["user_id"]);
                }
            }

        // To Get All Advanced Search Profiles: having searched us-zipcode/us-city and not in connections list
        $searchQuery = "SELECT * FROM members WHERE us_city='".$_POST["search"]."' OR us_zip='".$_POST["search"]."' && email!='".$_SESSION["email"]."' && id NOT IN('" . implode( "', '" , $connArray ) . "' )";
        
        if(isset($_POST["home_town"])){
            $searchQuery .= "
            AND city IN ('" . implode( "', '" , $_POST["home_town"] ) . "' )";
        }
        
        if(isset($_POST["language"])){
            $searchQuery .= "
            AND city IN ('" . implode( "', '" , $_POST["language"] ) . "' )";
        }
        
        if(isset($_POST["interest"])){
            $searchQuery .= "
            AND city IN ('" . implode( "', '" , $_POST["interest"] ) . "' )";
        }

            $result = $Conn->query($searchQuery);
            echo'<span> '.$result->num_rows.' Profile(s) found</span>';
            if ($result->num_rows > 0){
                while($rows = $result->fetch_assoc()){
                echo'
                <ul id="bestMatchData">
              
                <li>			
                    <div class="prof-wid150">';
                    if(empty($rows["image"])){
                        echo'<img src="Img/boy-small.png" alt="'.$rows["name"].'" title="'.$rows["name"].'" id="profile_icon"/>';
                    }else{
                        echo'<img src="Img/uploads/'.$rows["image"].'" alt="'.$rows["name"].'" title="'.$rows["name"].'" id="profile_icon"/>';
                    }
                    echo'    
                        
                    </div>
                    

                    <div class="prof-wid372 inner-prof-connect">
                        <div class="col-md-12">
                            <a href="javascript:void(0);"><h4>'.$rows["title"].' '.$rows["name"].'<span> ('.$rows["age"].' years)</span></h4></a>
                            <br/>
                            <span><h5 class="">'.$rows["us_city"].' '.$rows["us_state"].' '.$rows["us_zip"].'<br/></h5></span>
                        </div>
                        <div class="col-md-6 width50">
                            <h5 class="profile-street-home"> 
                            '.$rows["city"].'
                            </h5>                                                                  
                            <h5 class="profile-street profile-street-first-more">'.$rows["interests"].'</h5>                                                                                                                                
                        </div>
                        <div class="col-md-6 width50 padding10">
                            <h5 class="profile-speaks profile-street-first-more">'.$rows["languages"].'</h5>                                                              
                        </div>    

                    </div>
                    <div class="prof-wid182">
                        <form method="post">
                            <input type="hidden" name="user_id" value="'.$_SESSION["primary_id"].'" />
                            <input type="hidden" name="user_name" value="'.$_SESSION["primary_name"].'" />
                            <input type="hidden" name="connection_id" value="'.$rows["id"].'" />
                            <input type="hidden" name="connection_name" value="'.$rows["title"].' '.$rows["name"].'" />
                            <input type="hidden" name="connection_email" value="'.$rows["email"].'" />  
                            <button type="submit" name="connect" class="btn btn-default" title="CONNECT">CONNECT</button> 
                        </form>
                    </div>
                </li>			
            </ul>
                    ';
                }
            }
                
           
    }

	
?>