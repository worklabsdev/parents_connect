<div class="feedback-section"> 
    <a class="feedback-icon feedback-form" href="javascript:void(0);" data-toggle="modal" data-target="#myModal" title="Feedback">Feedback</a>
</div>

<div class="modal fade popupstyle" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                
                <h4 class="modal-title" id="myModalLabel"> Feedback</h4>
                <button type="button" class="close feedback-close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <p>Let us know what's working for you, and what's not working for you on Parents Connect! </p>
                <p id="feedback_succ_msg" class="class_hide"></p>
                <form method="post" accept-charset="utf-8" class="full-width form-horizontal my-account bv-form" id="user_feedback" name="user_feedback">
                    
                    <div class="form-fields width100">
                        <div class="form-group has-feedback has-error">
                            <label for="name" class="control-label">Name</label>
                            <div class="width75">
                                <input type="text" name="feedback_name" value="" maxLength="20" required id="feedback_name" class="form-control"><i class="form-control-feedback glyphicon glyphicon-remove" data-bv-icon-for="feedback_name" style="display: block;"></i> 
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="email" class="control-label">Email</label>
                            <div class="width75">
                                <input type="text" name="feedback_email" placeholder="Email (format: xxx@xxx.xxx)" title="Email (format: xxx@xxx.xxx)" TextMode="Email" validate="required:true" pattern="[a-zA-Z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*" value="<?php if(isset($_SESSION["email"])){echo $_SESSION["email"];}?>" <?php if(isset($_SESSION["email"])){echo 'disabled';}?> id="feedback_email" class="form-control" data-bv-field="feedback_email" autocomplete="new-password"><i class="form-control-feedback" data-bv-icon-for="feedback_email" style="display: none;"></i>                        
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="message" class="control-label">Message</label>
                            <div class="width75">
                                <textarea name="feedback_msg" required cols="40" rows="10" id="feedback_msg" data-bv-field="feedback_msg"></textarea><i class="form-control-feedback" data-bv-icon-for="feedback_msg" style="display: none;"></i>
                            </div>
                        </div>
                        
                    
                        <div class="width75 btn-sxn">
                            <button name="feedback_sub" type="submit" title="Submit" id="feedback_sub" class="btn btn-default button-sxn">SUBMIT</button>			  
                            <button name="cancel" type="button" title="Cancel" class="btn btn-default button-sxn grey-bg feedback-close" data-dismiss="modal">CANCEL</button>  
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


      