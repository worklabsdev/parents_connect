<?php
    session_start();
    if(isset($_SESSION['email'])){
        header('Location: profiles.php');    
    }

  
// Login
if (isset($_POST['login'])){
    require_once 'database/dbConfig.php';
    if ($_SERVER['REQUEST_METHOD'] == "POST"){
        $LoginSql = "SELECT * FROM users WHERE email = '".$_POST["email"]."' AND password = '".$_POST["password"]."'";
        $result = $Conn->query($LoginSql);
        if($result->num_rows > 0){
            $row = $result->fetch_assoc();
            $_SESSION['email'] = $row['email'];

            header('Location: profiles.php');   
            
            
        }else{
            echo'<script>alert("Email or Password not valid, please try again..")</script>';

        }
    }
}

include 'header.php';
?>


<section class=" equal" style="">
    <section class="equal">

            <div class="container registeration">

				
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group"> 
                        <div class="member-login col-md-8">
                            <i class="fa fa-user"></i>
                            <h3>Member Login</h3>
                        </div>
                    <form class="form" method="post" id="registrationForm">
				  
						
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" placeholder="User Email" name="email" required class="form-control" />
                                </div>
                            </div>
                        </div>
                      
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="password" placeholder="Password" name="password" required class="form-control" />
                                </div>
                            </div>
                        </div>
                      <div class="row">
                        <div class="col-md-4">
                            <a href="">Forgot Password?</a> 
                        </div>
                        <div class="col-md-4">
                            <input type="checkbox" />Remember me
                        </div>
                      </div>
                        <input type="submit" name="login" class="col-md-8 add btn btn-warning" value="LOGIN" />
                      </form>
                      <a href="register.php">Not registered yet? Sign up</a>
                  </div>
                </div>
              </div>
          </div>
    </section>
</section>

<?php
include 'footer.php';
?>
