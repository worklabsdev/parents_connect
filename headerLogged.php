<!doctype html>
<html lang="en">
   <head>
   <title>Parents Holiday</title>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <!-- Bootstrap CSS -->
      <link rel="icon" href="images/logo.png">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
      <link rel="stylesheet" href="css/stylesheet.css">
	  <link href="css/kompleter.css" rel="stylesheet" type="text/css" media="screen" />
	  <link href="css/jquery-ui.css" rel="stylesheet" type="text/css" media="screen" />
        <!-- <link href="css/frontend/style.css" rel="stylesheet" type="text/css"/>  -->
      <link rel="stylesheet" href="css/slick.css">
      <link rel="stylesheet" href="css/slick-theme.css">
      <link rel="stylesheet" href="css/bootstrap.css">
      <link rel="stylesheet" href="css/style.css">
      <!-- <link href="pagination/css/pagination.css" rel="stylesheet" type="text/css" />
<link href="pagination/css/A_green.css" rel="stylesheet" type="text/css" /> -->
      
   </head>
   <body>
<header class="position_static ">
         <nav class="navbar navbar-expand-lg navbar-light main-nav fixed-top " id="mainNav">
            <div class="container">
               <a class="navbar-brand js-scroll-trigger logo" href="index.php">
               <!-- <img src="images/logo.png"> -->
               PARENTS- -CONNECT
               </a>
               <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
               <span class="navbar-toggler-icon"></span>
               </button>
               <div class="collapse navbar-collapse row" id="navbarNavDropdown">
               <div class="col-md-12 menu_flex">
                           <ul class="navbar-nav ml-auto social-element">
                                
                                
                              <li class="nav-item ">
                                    <a href="tel:(470)2870655"><img src="images/flag-usa.png"> (470)2870655</a>
                              </li>
                              <li class="nav-item ">
                                    <a href="+917291010700"><img src="images/flag-ind.png"> (+91)7291010700</a>
                                    </li>
                                    <li class="nav-item ">
                                          <a class="nav-link js-scroll-trigger" href="best_match.php">Find Connections</a>
                                    </li>
                                    <li class="nav-item ">
                                          <a class="nav-link js-scroll-trigger" href="my_connections.php">My Connections</a>
                                    </li>
                                    <li class="nav-item ">
                                          <a class="nav-link js-scroll-trigger" href="profiles.php">Profiles</a>
                                    </li>
                                    <li class="nav-item ">
                                         <a class="nav-link js-scroll-trigger" href="account.php">Account</a>
                                    </li>
                                    <li class="nav-item ">
                                        <a class="nav-link js-scroll-trigger" href="logout.php">Logout</a>
                                    </li>
                                
                                
                              </ul>
                     </div>
                     <div class="col-md-12 menu_flex">
                     <!-- <ul class="navbar-nav ml-auto nav-element">
                        <li class="nav-item ">
                              <a class="nav-link " href="about_us.php">About us</a>
                           </li>
                     <li class="nav-item ">
                        <a class="nav-link " href="http://booking.parentsholiday.com/">Flights </a>
                     </li>
                     <li class="nav-item ">
                           <a class="nav-link " href="http://booking.parentsholiday.com/hotels/">Hotels </a>
                        </li>
                     <li class="nav-item dropdown ">
                        <a class="nav-link dropdown-toggle" href="packages.php" id="navbardrop" data-toggle="dropdown">Holiday Packages </a>
                        <div class="dropdown-menu">
                              <a  class="dropdown-item" href="http://booking.parentsholiday.com/india-packages.aspx">Domestic Packages</a>
                              <a class="dropdown-item" href="http://booking.parentsholiday.com/international-packages.aspx">International Packages</a>
                            </div>
                     </li>
                     <li class="nav-item ">
                           <a class="nav-link " href="http://booking.parentsholiday.com/bus/">Bus   </a>
                        </li>
                     
                     <li class="nav-item ">
                           <a class="nav-link " href="http://booking.parentsholiday.com/sightseeing/">Sightseeing  </a>
                        </li>
                        <li class="nav-item ">
                              <a class="nav-link " href="http://booking.parentsholiday.com/transfers/">Transfer  </a>
                           </li>
                           <li class="nav-item ">
                                 <a href="http://booking.parentsholiday.com/deals/" style="font-weight: bold;">
                                    <img style="vertical-align: text-bottom;" src="http://booking.parentsholiday.com/img/hot-deal-icon1.png">&nbsp;Hot Deals</a>
                              </li>
                              <li class="nav-item dropdown">
                                    <a class="nav-link " href="register.php" data-toggle="dropdown">Support    </a>
                                    <ul class="dropdown-menu" role="menu">
                                          <li><a class="dropdown-item" href="http://booking.parentsholiday.com/login.aspx?p=m">My booking</a></li>
                                          <li><a class="dropdown-item" href="http://booking.parentsholiday.com/enquiry.aspx">Send Enquiry</a></li>
                                          <li><a class="dropdown-item" href="http://booking.parentsholiday.com/makeonlinepayment.aspx">Make Payment</a></li>
                                      </ul>
                                 </li>
                                 
                                    
                                  
                    
                  </ul> -->
</div>
               </div>
            </div>
         </nav>
      </header>