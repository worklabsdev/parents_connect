<?php
session_start();
if(isset($_SESSION["email"])){
    include 'headerLogged.php';
}else{
    header('Location: login.php');
}
?>


<section class=" equal" style="">
    <section class="equal">
        <div class="container">
            <div class="row">
                <a class="btn btn-warning" title="ADD PROFILE" href="addProfile.php">ADD PROFILE</a>
            </div>
                   
<?php
        require_once 'database/dbConfig.php';

//To DEACTIVATE Profile
        if(isset($_POST['deactive'])){
            $id=$_POST["userId"];
                $deactivateQuery = "UPDATE members SET isEnabled=0 WHERE id='$id'";
                $Conn->query($deactivateQuery);
                if ($Conn->query($deactivateQuery) === TRUE) {
                    echo "<script>alert('Deactivated')</script>";
                }else{
                    echo "<script>alert('Some Problem Occured')</script>";
                }
        }

//To ACTIVATE Profile
        if(isset($_POST['active'])){
            $id=$_POST["userId"];
                $activateQuery = "UPDATE members SET isEnabled=1 WHERE id='$id'";
                $Conn->query($activateQuery);
                if ($Conn->query($activateQuery) === TRUE) {
                    echo "<script>alert('Activated')</script>";
                }else{
                    echo "<script>alert('Some Problem Occured')</script>";
                }
        }

//To Get All Profiles
            $profileQuery = "SELECT * FROM members WHERE email='".$_SESSION["email"]."'";
            $result = $Conn->query($profileQuery);
                                            
            if ($result->num_rows > 0){
                while($rows = $result->fetch_assoc()){
                echo'
                    <form method="post">
                        <input type="hidden" name="userId" value="'.$rows["id"].'" />
                        <div class="row add">
                            <div class="col-md-12">  
                                <div class="form-group">
                                    <h3 class="capitalize">'.$rows["title"].' '.$rows["name"].'</h3> 
                                    <div class="row">
                                        <div class="col-md-4">('.$rows["age"].' years)</div> 
                                        <div style="" class="col-md-4" id="spouse"><a class="btn btn-secondary" href="addProfile.php">+ Add Spouse</a></div>
                                        
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                US Address:
                                            </div>
                                        
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                '.$rows["us_address1"].', '.$rows["us_address2"].', '.$rows["us_city"].', '.$rows["us_zip"].'
                                            </div>
                                        
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                Speaks:
                                            </div>
                                        
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                '.$rows["languages"].'
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                Interests:
                                            </div>
                                        
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                '.$rows["interests"].'
                                            </div>
                                    
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                Home Town:
                                            </div>
                                    
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                '.$rows["state"].', '.$rows["city"].'
                                            </div>
                                    
                                        </div>
                                    </div>';
                                    if(!empty($rows["us_stay_from1"])){
                                    echo'<div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    US Arrival Date:
                                                </div>
                                        
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    '.$rows["us_stay_from"].'
                                                </div>
                                            
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    US Departure Date:
                                                </div>
                                            
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    '.$rows["us_stay_to"].'
                                                </div>
                                        
                                            </div>
                                        </div>
                                        ';
                                    }else{
                                    echo'<div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    Stay in USA:
                                                </div>
                                        
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    Always
                                                </div>
                                            
                                            </div>
                                        </div>';
                                    }
                                echo'<div class="row">
                                        <a href="edit_profile.php?userId='.$rows['id'].'" class="btn btn-warning edit" title="EDIT">EDIT</a>&nbsp;&nbsp;';
                                        if($rows["isEnabled"]==1){                                  
                                            echo '<input type="submit" name="deactive" id="deactive" class="btn btn-dark deactive" value="DEACTIVATE PROFILE" title="DEACTIVATE PROFILE" />';
                                        }else{
                                            echo '<input type="submit" name="active" id="active" class="btn btn-dark active" value="ACTIVATE PROFILE" title="ACTIVATE PROFILE" />';
                                        }
                                        echo'
                                    </div>
                                </div> 
                            </div> 
                        </div>
                    </form>';
                }
            }else{
                echo'<div class="alert alert-dark">Some Problem Occured... Try again later</div>';
            }
            echo'</section>
        </section>
    </div>';
include 'footer.php';
?>