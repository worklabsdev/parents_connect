<?php
    session_start();

    // if user is Logged In then it will include headerLogged file otherwise header file
    if(isset($_SESSION["email"])){
        include 'headerLogged.php';
    }else{
        include 'header.php';
    }
?>

<!-- Include this css externally as it was not accepting the style.css (included in header file) -->
<link href="css/frontend/style.css" rel="stylesheet" type="text/css"/> 

<!-- Included Feedback File -->
<?php include 'feedback.php'; ?>

<!-- On submitting Feedback Form -->
<?php
    require_once 'database/dbConfig.php';   
    if (isset($_POST['feedback_sub'])) {
        if ($_SERVER['REQUEST_METHOD'] == "POST"){
            $sql = "INSERT INTO feedback VALUES(null, '".$_POST["feedback_name"]."','".$_POST["feedback_email"]."','".$_POST["feedback_msg"]."')";

            if ($Conn->query($sql) === TRUE) {
                echo '<script>alert("Thanks for your feedback")</script>';
            }else{
                echo '<script>alert("Some Problem Occured.. Try Again Later")</script>';
            }
        }
    }
?>


<section class=" equal" style="">
    <section class="equal">
        <div class="container">
            <div class="">
                <h3>Helping US Visitors</h3>
                <h1><strong>Get Connected</strong></h1>
                <hr>
                <h5>with other visitors in the neighborhood</h5>
                <hr>
            </div>
        </div>
    </section>
</section>

<!-- Included Footer -->
<?php include 'footer.php'; ?>