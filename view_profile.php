<?php
session_start();
if(isset($_SESSION["email"])){
    include 'headerLogged.php';
}else{
    header('Location: login.php');
}
?>


<section class=" equal" style="">
    <section class="equal">
        <div class="container">
                   
<?php
        require_once 'database/dbConfig.php';

//To Get Profile Details
            $profileQuery = "SELECT * FROM members WHERE id='".$_GET["user"]."'";
            $result = $Conn->query($profileQuery);
                                            
            if ($result->num_rows > 0){
                $rows = $result->fetch_assoc();
                    echo'
                        <div class="row add">
                            <div class="col-md-12">  
                                <div class="form-group">
                                    <h3 class="capitalize">'.$rows["title"].' '.$rows["name"].'</h3> 
                                    <div class="row">
                                        <div class="col-md-4">('.$rows["age"].' years)</div>                        
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                US Address:
                                            </div>
                                        
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                '.$rows["us_address1"].', '.$rows["us_address2"].', '.$rows["us_city"].', '.$rows["us_zip"].'
                                            </div>
                                        
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                Contact Email:
                                            </div>
                                        
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                '.$rows["email"].'
                                            </div>
                                        
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                Speaks:
                                            </div>
                                        
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                '.$rows["languages"].'
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                Interests:
                                            </div>
                                        
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                '.$rows["interests"].'
                                            </div>
                                    
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                Home Town:
                                            </div>
                                    
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                '.$rows["state"].', '.$rows["city"].'
                                            </div>
                                    
                                        </div>
                                    </div>';
                                    if(!empty($rows["us_stay_from1"])){
                                    echo'<div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    US Arrival Date:
                                                </div>
                                        
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    '.$rows["us_stay_from"].'
                                                </div>
                                            
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    US Departure Date:
                                                </div>
                                            
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    '.$rows["us_stay_to"].'
                                                </div>
                                        
                                            </div>
                                        </div>
                                        ';
                                    }else{
                                    echo'<div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    Stay in USA:
                                                </div>
                                        
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    Always
                                                </div>
                                            
                                            </div>
                                        </div>';
                                    }
                                echo'
                                </div> 
                            </div> 
                        </div>';
            }else{
                echo'<div class="alert alert-dark">Some Problem Occured... Try again later</div>';
            }
            echo'</section>
        </section>
    </div>';
include 'footer.php';
?>