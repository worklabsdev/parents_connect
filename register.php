<?php
 include 'header.php';
?>


<section class=" equal" style="">
    <section class="equal">

            <div class="container registeration">

				      <h4>Join Now</h4>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group"> 

                    <form class="form" method="post" id="registrationForm">
				  
						
                        <div class="row">
                          <div class="col-md-4">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <select name="title" id="title" class="form-control title" required onchange="titleChange(this.value)">
                                  <option value="">--</option>
                                  <option value="Ms.">Ms.</option>
                                  <option value="Mr.">Mr.</option>
                                  <option value="Mrs.">Mrs.</option>
                                </select>
                                
                            </div>
                          </div>
                      
                        <div class="col-md-8">
                          <div class="form-group">                         
                              <label for="name">Name*</label>
                                <input type="text" id="name" class="form-control name" required value="" maxLength="20" name="name" onkeyup="nameChange(this.value)" placeholder="Name of the person registering the account">
                              </div>
                          </div>
                        </div>
                        <div class="row">
                        <div class="col-md-12">
                                        <div class="form-group">
                                          <label for="email">Your Email*</label>
                                          <input type="text" id="email" class="form-control" required name="email" value="" placeholder="Email (format: xxx@xxx.xxx)" title="Email (format: xxx@xxx.xxx)" TextMode="Email" validate="required:true" pattern="[a-zA-Z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*">
                                      </div>
                                    </div>
                                    </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">                         
                                        <label for="address2">Password*</label>
                                          <input type="password" placeholder="xxxxxx (6-10 words)" class="form-control" minLength="6" maxLength="10" required name="password" id="password" value="">
                                        </div>
                                    </div>
                        </div>
                        
                        <h5>Setup Profiles for</h5>
                        <input type="radio" name="myRadios" onchange="handleChange1();" required /> My Self &nbsp;
                        <input type="radio" name="myRadios" onchange="handleChange2();" /> My Parents &nbsp;
                        <input type="radio" name="myRadios" onchange="handleChange3();" /> My In-Laws

                        <div id="profiles"></div>
                        <input type="submit" id="apni" name="register1" class="add add_details register1" value="Next" />
                      </form>
                  </div>
                </div>
              </div>
          </div>
                

 </section>
</section>
<?php
  include 'footer.php';
?>
<script>


// To change both name values (self) on change of upper or lower name fields
  function nameChange(ele){ 
    $('.name').val(ele);
    
  }

// To change both title values (self) on change of upper or lower title selectors
  function titleChange(ele){ 
    $('.title').val(ele);
    
  }


// Making profile for self

  function handleChange1(){ 
      $('#profiles').html(`<div class="row">
						            <div class="col-md-3">
                            <div class="form-group">
                            <input type="hidden" name="profileType" id="profileType" value="self" />
                              <label for="title">Title</label>
                              <select name="title1" id="title1" required class="form-control title" required onchange="titleChange(this.value)">
                                <option value="">--</option>
                                <option value="Ms.">Ms.</option>
                                <option value="Mr.">Mr.</option>
                                <option value="Mrs.">Mrs.</option>
                              </select>
                              
                            </div>
                        </div>
          
						            <div class="col-md-6">
							              <div class="form-group">                         
                             <label for="name">Name*</label>
                              <input type="text" id="name1" required class="name form-control" value="" onkeyup="nameChange(this.value)" required name="name1">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                              <label for="title">Age</label>
                              <select id="age1" name="age1" required class="form-control">
                              <option value="">--</option>
                              </select>
                              
                            </div>
                        </div>
					  </div>

            <h5>Add Picture</h5>
            <span id="uploaded_image"><img src="Img/uploads/upload-img.jpg" class="avatar img-circle img-thumbnail" alt="avatar" />
            <input type="file" name="file" id="file" /></span>

            <div class="add" id="spouse"><input type="button" id="addSpouse" class="btn btn-grey" value="+ Add Spouse"/></div>

						`);
            
            for(i=18; i<100; i++){
              $("#age1").append("<option value="+i+">" + i + "</option>");
            }
    
  }


// Making profile for Parents

  function handleChange2(){ 
      $('#profiles').html(`<div class="row">
						            <div class="col-md-3">
                            <div class="form-group">
                            <input type="hidden" name="profileType" id="profileType" value="parents" />
                              <label for="title">Title</label>
                              <select name="title1" id="title1" required class="form-control">
                                <option value="">--</option>
                                <option value="Ms.">Ms.</option>
                                <option value="Mr.">Mr.</option>
                                <option value="Mrs.">Mrs.</option>
                              </select>
                              
                            </div>
                        </div>
          
						            <div class="col-md-6">
							              <div class="form-group">                         
                             <label for="name">Name*</label>
                              <input type="text" id="name1" required class="form-control" value="" name="name1">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                              <label for="title">Age</label>
                              <select id="age1" name="age1" required class="form-control">
                              <option value="">--</option>
                              </select>
                              
                            </div>
                        </div>
					  </div>

            <h5>Add Picture</h5>
            <span id="uploaded_image"><img src="Img/uploads/upload-img.jpg" class="avatar img-circle img-thumbnail" alt="avatar" />
            <input type="file" name="file" id="file" /></span>

            <div class="add" id="parent"><input type="button" id="addParent" class="btn btn-grey" value="+ Add Parent"/></div>

						`);
            
            for(i=18; i<100; i++){
              $("#age1").append("<option value="+i+">" + i + "</option>");
            }
    
  }


// Making profile for In-Laws

  function handleChange3(){ 
      $('#profiles').html(`<div class="row">
						            <div class="col-md-3">
                            <div class="form-group">
                            <input type="hidden" name="profileType" id="profileType" value="inlaws" />
                              <label for="title">Title</label>
                              <select name="title1" id="title1" required class="form-control">
                                <option value="">--</option>
                                <option value="Ms.">Ms.</option>
                                <option value="Mr.">Mr.</option>
                                <option value="Mrs.">Mrs.</option>
                              </select>
                              
                            </div>
                        </div>
          
						            <div class="col-md-6">
							              <div class="form-group">                         
                             <label for="name">Name*</label>
                              <input type="text" id="name1" required class="form-control" value="" name="name1">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                              <label for="title">Age</label>
                              <select id="age1" name="age1" required class="form-control">
                              <option value="">--</option>
                              </select>
                              
                            </div>
                        </div>
					  </div>

            <h5>Add Picture</h5>
            <span id="uploaded_image"><img src="Img/uploads/upload-img.jpg" class="avatar img-circle img-thumbnail" alt="avatar" />
            <input type="file" name="file" id="file" /></span>

            <div class="add" id="inLaw"><input type="button" id="addInLaw" class="btn btn-grey" value="+ Add In-Laws"/></div>

						`);
            
            for(i=18; i<100; i++){
              $("#age1").append("<option value="+i+">" + i + "</option>");
            }
    
  }

  

</script>
<script>
  $(document).ready(function(){

// For uploading Profile Pic for Ist Person

    $(document).on('change', '#file', function(){
      var name = document.getElementById("file").files[0].name;
      var form_data = new FormData();
      var ext = name.split('.').pop().toLowerCase();
      if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1) 
      {
      alert("Invalid Image File");
      }
      var oFReader = new FileReader();
      oFReader.readAsDataURL(document.getElementById("file").files[0]);
      var f = document.getElementById("file").files[0];
      var fsize = f.size||f.fileSize;
      if(fsize > 2000000)
      {
      alert("Image file size is very big");
      }
      else
      {
        form_data.append("file", document.getElementById('file').files[0]);
        $.ajax({
          url:"upload.php",
          method:"POST",
          data: form_data,
          contentType: false,
          cache: false,
          processData: false,
          beforeSend:function(){
            $('#uploaded_image').html("<label class='text-success'>Image Uploading...</label>");
          },   
          success:function(data)
          {
            $('#uploaded_image').html(data);
          }
        });
      }
    });


// For uploading Profile Pic for 2nd Person

    $(document).on('change', '#file2', function(){
      var name2 = document.getElementById("file2").files[0].name;
      var form_data2 = new FormData();
      var ext2 = name2.split('.').pop().toLowerCase();
      if(jQuery.inArray(ext2, ['gif','png','jpg','jpeg']) == -1) 
      {
      alert("Invalid Image File");
      }
      var oFReader2 = new FileReader();
      oFReader2.readAsDataURL(document.getElementById("file2").files[0]);
      var f2 = document.getElementById("file2").files[0];
      var fsize2 = f2.size||f2.fileSize;
      if(fsize2 > 2000000)
      {
      alert("Image file size is very big");
      }
      else
      {
      form_data2.append("file2", document.getElementById('file2').files[0]);
      $.ajax({
        url:"upload2.php",
        method:"POST",
        data: form_data2,
        contentType: false,
        cache: false,
        processData: false,
        beforeSend:function(){
        $('#uploaded_image2').html("<label class='text-success'>Image Uploading...</label>");
        },   
        success:function(data)
        {
        $('#uploaded_image2').html(data);
        }
      });
      }
    });


        
// For adding Spouse

          $(document).on('click', '#addSpouse', function(){
            $('#spouse').html(`<div class="row">
						            <div class="col-md-3">
                            <div class="form-group">
                              <label for="title">Title</label>
                              <select name="title2" id="title2" required class="form-control">
                                <option value="">--</option>
                                <option value="Ms.">Ms.</option>
                                <option value="Mr.">Mr.</option>
                                <option value="Mrs.">Mrs.</option>
                              </select>
                              
                            </div>
                        </div>
          
						            <div class="col-md-6">
							              <div class="form-group">                         
                             <label for="name">Name*</label>
                              <input type="text" id="name2" required class="form-control" value="" name="name2">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                              <label for="title">Age</label>
                              <select id="age2" name="age2" required class="form-control">
                              <option value="">--</option>
                              </select>
                              
                            </div>
                        </div>
					  </div>

            <h5>Add Picture</h5>
            <span id="uploaded_image2"><img src="Img/uploads/upload-img.jpg" class="avatar img-circle img-thumbnail" alt="avatar" />
            <input type="file" name="file2" id="file2" /></span>

            <div class="add"><input type="button" class="removeSpouse btn btn-grey" value="- Remove Spouse"/></div>

						`);
            for(i=18; i<100; i++){
    $("#age2").append("<option value="+i+">" + i + "</option>");
  }
            
        });
        
// For Removing Spouse
        
        $(document).on('click', '.removeSpouse', function(){
          $('#spouse').html(`<input type="button" id="addSpouse" class="btn btn-grey" value="+ Add Spouse"/>`);
        });


// For adding Parent
        
          $(document).on('click', '#addParent', function(){
            $('#parent').html(`<div class="row">
						            <div class="col-md-3">
                            <div class="form-group">
                              <label for="title">Title</label>
                              <select name="title2" id="title2" required class="form-control">
                                <option value="">--</option>
                                <option value="Ms.">Ms.</option>
                                <option value="Mr.">Mr.</option>
                                <option value="Mrs.">Mrs.</option>
                              </select>
                              
                            </div>
                        </div>
          
						            <div class="col-md-6">
							              <div class="form-group">                         
                             <label for="name">Name*</label>
                              <input type="text" id="name2" class="form-control" required value="" name="name2">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                              <label for="title">Age</label>
                              <select id="age2" name="age2" required class="form-control">
                              <option value="">--</option>
                              </select>
                              
                            </div>
                        </div>
					  </div>

            <h5>Add Picture</h5>
            <span id="uploaded_image2"><img src="Img/uploads/upload-img.jpg" class="avatar img-circle img-thumbnail" alt="avatar" />
            <input type="file" name="file2" id="file2" /></span>

            <div class="add"><input type="button" class="removeParent btn btn-grey" value="- Remove Parent"/></div>

						`);
            for(i=18; i<100; i++){
    $("#age2").append("<option value="+i+">" + i + "</option>");
  }
            
        });
        
// For Removing Parent
        
        $(document).on('click', '.removeParent', function(){
          $('#parent').html(`<input type="button" id="addParent" class="btn btn-grey" value="+ Add Parent"/>`);
        });


// For adding In-Laws
        
          $(document).on('click', '#addInLaw', function(){
            $('#inLaw').html(`<div class="row">
						            <div class="col-md-3">
                            <div class="form-group">
                              <label for="title">Title</label>
                              <select id="title2" name="title2" required class="form-control">
                                <option value="">--</option>
                                <option value="Ms.">Ms.</option>
                                <option value="Mr.">Mr.</option>
                                <option value="Mrs.">Mrs.</option>
                              </select>
                              
                            </div>
                        </div>
          
						            <div class="col-md-6">
							              <div class="form-group">                         
                             <label for="name">Name*</label>
                              <input type="text" class="form-control" value="" required name="name" id="name2">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                              <label for="title">Age</label>
                              <select id="age2" name="age2" required class="form-control">
                              <option value="">--</option>
                              </select>
                              
                            </div>
                        </div>
					  </div>

            <h5>Add Picture</h5>
            <span id="uploaded_image2"><img src="Img/uploads/upload-img.jpg" class="avatar img-circle img-thumbnail" alt="avatar" />
            <input type="file" name="file2" id="file2" /></span>

            <div class="add"><input type="button" class="removeInLaw btn btn-grey" value="- Remove In-Laws"/></div>

						`);
            for(i=18; i<100; i++){
    $("#age2").append("<option value="+i+">" + i + "</option>");
  }
            
        });
        
// For Removing In-Laws
        
        $(document).on('click', '.removeInLaw', function(){
          $('#inLaw').html(`<input type="button" id="addInLaw" class="btn btn-grey" value="+ Add In-Laws"/>`);
        });



});

//For removing profile pic of 1st Person

function removeImage1(){
  $('#uploaded_image').html(`<img src="Img/uploads/upload-img.jpg" class="avatar img-circle img-thumbnail" alt="avatar" />
            <input type="file" name="file" id="file" />`);
}


//For removing profile pic of 2nd Person

function removeImage2(){
  $('#uploaded_image2').html(`<img src="Img/uploads/upload-img.jpg" class="avatar img-circle img-thumbnail" alt="avatar" />
            <input type="file" name="file2" id="file2" />`);
}


// On selecting the stay as 'During Stay in US' for 1st person
    function stayChange1_1(){ 
      $('#stay1').html(`
      <div class="row">
        <div class="col-md-6">
            <div class="form-group">
            
                <input type="text" id="from" name="us_stay_from1" class="form-control" required placeholder="From" value="">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
            
            <input type="text" id="to" name="us_stay_to1" class="form-control" required placeholder="To" value="">
            </div>
        </div>      
    </div>      
    `);
    }

// On selecting the stay as 'Always' for 1st person
    function stayChange2_1(){ 
      $('#stay1').html("");
    }


// On selecting the stay as 'During Stay in US' for 2nd person
    function stayChange1_2(){ 
      $('#stay2').html(`
      <div class="row">
        <div class="col-md-6">
            <div class="form-group">
            
                <input type="text" id="from2" name="us_stay_from2" class="form-control" required placeholder="From" value="">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
            
            <input type="text" id="to2" name="us_stay_to2" class="form-control" required placeholder="To" value="">
            </div>
        </div>      
    </div>      
    `);
    }

// On selecting the stay as 'Always' for 2nd person
    function stayChange2_2(){ 
      $('#stay2').html("");
    }
        
</script>




<?php

// For submitting 1st step of registeration
if(isset($_POST['register1'])){
  echo '<script type="text/javascript">
  $(".registeration").html(`<div id="loading" style="" ></div>`);';
  if(isset($_POST["title"])){
    echo 'var action = "store_data";
        ';
  }
  
  
  echo 'var title="'.$_POST["title"].'";
  
  var name="'.$_POST["name"].'";
  var email="'.$_POST["email"].'";
  var password="'.$_POST["password"].'";
  var title1="'.$_POST["title1"].'";
  var name1="'.$_POST["name1"].'";
  var age1="'.$_POST["age1"].'";
  var profileType="'.$_POST["profileType"].'";
  ';
  if(isset($_POST["image1"])){
    echo 'var image1="'.$_POST["image1"].'";
    ';
  }else{
    echo 'var image1="";
    ';
  }
  
  
if(isset($_POST["title2"])){
  echo 'var title2="'.$_POST["title2"].'";
  var name2="'.$_POST["name2"].'";
  var age2="'.$_POST["age2"].'";
  ';
  if(isset($_POST["image2"])){
    echo 'var image2="'.$_POST["image2"].'";
    ';
  }else{
    echo 'var image2="";
    ';
  }
}else{
  echo 'var title2="";
  var name2="";
  var age2="";
  var image2="";
  ';
}
 
echo '
  
  if ("geolocation" in navigator) 
  {
      request_location();
  }else{

    var lat=0;
    var long=0;

    $.ajax({
      url:"register1.php",
      method:"POST",
      data:{action:action, title:title, name:name, email:email, password:password, title1:title1, name1:name1, age1:age1,profileType:profileType, image1:image1, title2:title2, name2:name2, age2:age2, image2:image2, lat:lat, long:long},
      success:function(data){ 
          $(".registeration").html(data);
      }
    });
  } 

  function request_location()
  {
      setTimeout(request_location, 1000*60*2);

      navigator.geolocation.getCurrentPosition(update_location);
  }

  function update_location(position)
  {
    var latitude = position.coords.latitude;
    var longitude = position.coords.longitude;

    $.ajax({
      url:"register1.php",
      method:"POST",
      data:{action:action, title:title, name:name, email:email, password:password, title1:title1, name1:name1, age1:age1,profileType:profileType, image1:image1, title2:title2, name2:name2, age2:age2, image2:image2, lat:latitude, long:longitude},
      success:function(data){ 
          $(".registeration").html(data);
      }
    });
  }

  </script>';
 }
//  Ends 1st step of registeration


// For submitting 2nd step of registeration
if(isset($_POST['register2'])){
  echo '<script type="text/javascript">
  $(".registeration").html(`<div id="loading" style="" ></div>`);
  var action2 = "store_data";
  ';
  $languages1=implode(', ',$_POST["languages1"]);
  $interests1=implode(', ',$_POST["interests1"]);
  
  echo 'var languages1="'.$languages1.'";
   var interests1="'.$interests1.'";
   var title1="'.$_POST["title1"].'";
  var name1="'.$_POST["name1"].'";
  ';
  if(isset($_POST["languages2"])){
    $languages2=implode(', ',$_POST["languages2"]);
    $interests2=implode(', ',$_POST["interests2"]);
  echo'
    var languages2="'.$languages2.'";
    var interests2="'.$interests2.'";
    var title2="'.$_POST["title2"].'";
    var name2="'.$_POST["name2"].'";
  ';
  }else{
    echo'var languages2="";
    var interests2="";
    var title2="";
    var name2="";
  ';
  }

  echo'
  $.ajax({
      url:"register2.php",
      method:"POST",
      data:{action2:action2, languages1:languages1, interests1:interests1, languages2:languages2, interests2:interests2, title1:title1, title2:title2, name1:name1, name2:name2},
      success:function(data){ 
          $(".registeration").html(data);
      }
  });
     </script>';
 }
//  Ends 2nd step of registeration


// For submitting 3rd step of registeration
if(isset($_POST['register3'])){
  echo '<script type="text/javascript">
        $(".registeration").html(`<div id="loading" style="" ></div>`);
            var action3 = "store_data";
            
            var country1="'.$_POST["country1"].'";
            var state1="'.$_POST["state1"].'";
            var city1="'.$_POST["city1"].'";
            var us_address1_1="'.$_POST["us_address1_1"].'";
            var us_address2_1="'.$_POST["us_address2_1"].'";
            var us_zip1="'.$_POST["us_zip1"].'";
            var us_city1="'.$_POST["us_city1"].'";
            ';
            if(isset($_POST["us_stay_from1"])){
                echo'
                var us_stay_from1="'.$_POST["us_stay_from1"].'";
                var us_stay_to1="'.$_POST["us_stay_to1"].'";
                var us_stay_always1="0";
                ';
            }else{
                echo'
                var us_stay_from1="";
                var us_stay_to1="";
                var us_stay_always1="1";
              ';
            }


          if(isset($_POST["country2"])){
              echo'
              var country2="'.$_POST["country2"].'";
              var state2="'.$_POST["state2"].'";
              var city2="'.$_POST["city2"].'";
              var us_address1_2="'.$_POST["us_address1_2"].'";
              var us_address2_2="'.$_POST["us_address2_2"].'";
              var us_zip2="'.$_POST["us_zip2"].'";
              var us_city2="'.$_POST["us_city2"].'";
              ';
              if(isset($_POST["us_stay_from1"])){
                  echo'
                  var us_stay_from2="'.$_POST["us_stay_from2"].'";
                  var us_stay_to2="'.$_POST["us_stay_to2"].'";
                  var us_stay_always2="0";
                ';
              }else{
                  echo'
                  var us_stay_from2="";
                  var us_stay_to2="";
                  var us_stay_always2="1";
                  ';
              }
          
          }else{
            echo'var country2="";
            var state2="";
            var city2="";
            var us_address1_2="";
            var us_address2_2="";
            var us_zip2="";
            var us_city2="";
            var us_stay_from2="";
            var us_stay_to2="";
            var us_stay_always2="0";
          ';
          }

  echo'
  $.ajax({
      url:"register3.php",
      method:"POST",
      data:{action3:action3, country1:country1, state1:state1, city1:city1, us_address1_1:us_address1_1, us_address2_1:us_address2_1, us_zip1:us_zip1, us_city1:us_city1, us_stay_from1:us_stay_from1, us_stay_to1:us_stay_to1, us_stay_always1:us_stay_always1, country2:country2, state2:state2, city2:city2, us_address1_2:us_address1_2, us_address2_2:us_address2_2, us_zip2:us_zip2, us_city2:us_city2, us_stay_from2:us_stay_from2, us_stay_to2:us_stay_to2, us_stay_always2:us_stay_always2},
      success:function(data){ 
          $(".registeration").html(data);
      }
  });
     </script>';
 }
//  Ends 3rd step of registeration

?>

