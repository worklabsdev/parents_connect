<?php
session_start();


//On Sending Connection Request To Profiles
if (isset($_POST['connect'])) {
    require_once 'database/dbConfig.php';
    if ($_SERVER['REQUEST_METHOD'] == "POST"){
            $sql = "INSERT INTO connections VALUES(null, '".$_SESSION["email"]."','".$_POST["user_id"]."','".$_POST["user_name"]."','".$_POST["connection_email"]."','".$_POST["connection_id"]."','".$_POST["connection_name"]."', 'Sent',null )";
            if ($Conn->query($sql) === TRUE) {
                echo'<script>alert("Request Sent")</script>';
            }
        }
    }

//Included Header
include 'headerLogged.php';
?>   
<link href="css/frontend/style.css" rel="stylesheet" type="text/css"/>        
<section class="container-fluid add-profile"><!--get-connected-->
        <div class="row">
            <div class="col-md-12 width767 width1600 padding0 best-search-result">
                <div class="full-width margin-top50">
                    <div class="top-links">
                        <ul class="full-width">
                            <li>
                                <a href="best_match.php" title="Best Matches">Best Matches</a>
                            </li>
                            <li class="active-search">
                                <a href="search.php" title="Advanced Search">Advanced Search</a>                            
                            </li>
                        </ul>
					    <div class="search-results scroll-area">
                            <div id="pro-near">							
                                <div class="search-area full-width">
                                                                        	
                                      
                                    <form class="full-width form-horizontal" method="post" action="javascript:void(0);">
                                        <div class="form-fields width100">
                                        
                                            <div class="full-width custom-form custom-form-2">                                        
                                                <div class="row">
                                                    <div class="col-lg-12  text-center">
                                                        <label class="custome-label sear-label">Search Connections in: </label> 
                                                        <div class="form-group  has-feedback search-box">  
                                                            <input type="text" name="search_zipcode" value="<?php if(isset($_GET["search"])){echo$_GET["search"];}?>" id="search_zip_city" placeholder="Enter City or Zip Code" class="form-control advance_filter">
                                                        </div> 
                                                    </div>
                                                </div> 
                                            </div>
                                        
                                                                            
                                            <div class="full-width">
                                                <div class="row vertical-align form-filter form-filter-2">
                                                    <div class="col-sm-2 text-center">
                                                            <label class="custome-label-2">Filters:</label>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group has-feedback search-box">
                                                            <input type="text" name="search_home_town_visible" value="" id="search_home_town" placeholder="Home Town" class="form-control ui-autocomplete-input" autocomplete="off">
                                                        </div>
                                                        <div class="cmn-filter">
                                                            <ul class="home_town">
                                                            </ul>
                                                        </div>
                                                        <div id="show_home_town">
                                                        </div>
                                                    </div>
                                        
                                        
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <input type="text" name="search_language_visible" value="" id="search_language" placeholder="Languages" class="form-control filter-height ui-autocomplete-input" autocomplete="off">
                                                            <input type="hidden" name="search_language" id="hidden_search_language" value="" autocomplete="new-password">
                                            
                                                            <div class="cmn-filter">
                                                                <ul class="language"></ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                            
                                            
                                                    <div class="col-sm-3">
                                                        <div> 
                                                            <div class="form-group">
                                                                <input type="text" name="search_interest_visible" value="" id="search_interest" placeholder="Interests" class="form-control filter-height ui-autocomplete-input" autocomplete="off">          
                                                                <input type="hidden" name="search_interest" id="hidden_search_interest" value="" autocomplete="new-password">
                                                    
                                                                <div class="cmn-filter">
                                                                    <ul class="interest"></ul>
                                                                </div> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>                              
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <div class="profiles-connect">	
                                    
                                </div>
                            </div>					
                        </div>
                        <div id="mapfeeds">
                                                           
                                <div class="search-results second-child">
                                    <div class="map-sxn">
                                        <div class="map-section" id="obj1">  
                                            
                                        <a id="mapData"></a>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<!-- Modal -->

<?php include 'footer.php'; ?>

<script>

$(document).ready(function(){
    
//To Get the BEST MATCH for Particular Profile   
    filter_data();
    function filter_data(){
        $('.profiles-connect').html('<div id="loading"></div>');
        var action = 'fetch_data';
        var search = $("#search_zip_city").val();
        var home_town = get_filter('home');
        var language = get_filter('lang');
        var interest = get_filter('int');
       
        $.ajax({
            url:"fetch_advanced_search.php",
            method:"POST",
            data:{action:action, search:search, home_town:home_town, language:language, interest:interest},
            success:function(data){
                $('.profiles-connect').html(data);
            }
        });

        function get_filter(class_name){
            var filter = [];
            $('.'+class_name+'').each(function(){
                filter.push($(this).val());
            });
            return filter;
        }

        $.ajax({
            url:"fetch_map_data.php",
            method:"POST",
            data:{action:action, search:search},
            success:function(data){
                $('#mapData').html(data);
            }
        });


    }

    $(document).on("keyup", "#search_zip_city", function(){
        filter_data();
    
    });


//To add home town in filters
    var k=1;
    $("#search_home_town").on("keydown",function search(e) {
        if(e.keyCode == 13) {
            k++;
            $(".home_town").append(`
                <li id="home_`+k+`">
                    <span>
                        `+$(this).val()+` 
                    </span>
                    <input type="hidden" name="home_towns[]" class="home" value="`+$(this).val()+`" />
                    <a href="javascript:void(0);" id="`+k+`" class="home_remove"> 
                        <img src="Img/icon-close.png" alt="">
                    </a>
                </li>`);
                $("#search_home_town").val("");
                filter_data();
        }
    });

    
    $(document).on("click", ".home_remove", function(){
        var home_id = $(this).attr("id"); 
        $("#home_"+home_id+"").remove();
        filter_data();
    });


//To add Languages in filters
    var l=1;
    $("#search_language").on("keydown",function search(e) {
        if(e.keyCode == 13) {
            l++;
            $(".language").append(`
                <li id="language_`+l+`">
                    <span>
                        `+$(this).val()+` 
                    </span>
                    <input type="hidden" name="languages[]" class="lang" value="`+$(this).val()+`" />
                    <a href="javascript:void(0);" id="`+l+`" class="language_remove"> 
                        <img src="Img/icon-close.png" alt="">
                    </a>
                </li>`);
                $("#search_language").val("");
                filter_data();
        }
    });

    
    $(document).on("click", ".language_remove", function(){
        var language_id = $(this).attr("id"); 
        $("#language_"+language_id+"").remove();
        filter_data();
    });


//To add Interests in filters
    var m=1;
    $("#search_interest").on("keydown",function search(e) {
        if(e.keyCode == 13) {
            m++;
            $(".interest").append(`
                <li id="interest_`+m+`">
                    <span>
                        `+$(this).val()+` 
                    </span>
                    <input type="hidden" name="interests[]" class="int" value="`+$(this).val()+`" />
                    <a href="javascript:void(0);" id="`+m+`" class="interest_remove"> 
                        <img src="Img/icon-close.png" alt="">
                    </a>
                </li>`);
                $("#search_interest").val("");
                filter_data();
        }
    });

    
    $(document).on("click", ".interest_remove", function(){
        var interest_id = $(this).attr("id"); 
        $("#interest_"+interest_id+"").remove();
        filter_data();
    
    });

});
</script>

    

