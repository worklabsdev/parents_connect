<?php
session_start();

//On Sending Connection Request To Profiles
if (isset($_POST['connect'])) {
    require_once 'database/dbConfig.php';
    if ($_SERVER['REQUEST_METHOD'] == "POST"){
            $sql = "INSERT INTO connections VALUES(null, '".$_SESSION["email"]."','".$_POST["user_id"]."','".$_POST["user_name"]."','".$_POST["connection_email"]."','".$_POST["connection_id"]."','".$_POST["connection_name"]."', 'Sent',null )";
            if ($Conn->query($sql) === TRUE) {
                echo'<script>alert("Request Sent")</script>';
            }
        }
    }

//Included Header
include 'headerLogged.php';
?>   
<link href="css/frontend/style.css" rel="stylesheet" type="text/css"/>        
<section class="container-fluid add-profile"><!--get-connected-->
        <div class="row">
            <div class="col-md-12 width767 width1600 padding0 best-search-result">
                <div class="full-width margin-top50">
                    <div class="top-links">
                        <ul class="full-width">
                            <li class="active-search">
                                <a href="best_match.php" title="Best Matches">Best Matches</a>
                            </li>
                            <li>
                                <a href="search.php" title="Advanced Search">Advanced Search</a>                            
                            </li>
                        </ul>
					    <div class="search-results scroll-area">
                            <div id="pro-near">							
                                <div class="search-area full-width">
                                                                        	
                                      
                                    <form class="full-width form-horizontal" action="javascript:void(0);">
                                        <div class="form-fields width100">
                                            <div class="form-group">
                                                <label class="control-label">View Suggestions for</label>
                                                <div class="width75">
                                                    <div class="select-box">
                                                        <select class="select profiles" name="profile_name" id="profile_name" >
                                                            <?php
                                                                require_once 'database/dbConfig.php';

                                                                //To Get All Profiles
                                                                $profileQuery = "SELECT * FROM members WHERE email='".$_SESSION["email"]."'";
                                                                $result = $Conn->query($profileQuery);
                                                                                                
                                                                if ($result->num_rows > 0){
                                                                    while($rows = $result->fetch_assoc()){
                                                                        echo'<option class="profiles" value="'.$rows["id"].','.$rows["title"].' '.$rows["name"].','.$rows["lat"].','.$rows["long"].'">'.$rows["title"].' '.$rows["name"].' - '.$rows["us_city"].' - '.$rows["us_zip"].'</option>';
                                                                    }
                                                                }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>&nbsp;
                                                <a href="addProfile.php" class="fa fa-user-plus">Add</a>
                                            </div>	   
                                        </div>
                                    </form>
                                </div>

                                <h3 class="head-class"> Search Results</h3>

                                <div class="profiles-connect">	
                                    
                                </div>
                            </div>					
                        </div>
                        <div id="mapfeeds">                       
                            <div class="search-results second-child">
                                <div class="map-sxn">
                                    <div class="map-section" id="obj1">  
                                        <a id="mapData"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<!-- Modal -->

<?php include 'footer.php'; ?>

<script>

//To Get the BEST MATCH for Particular Profile
$(document).ready(function(){
 
    filter_data();
    function filter_data(){
        $('.profiles-connect').html('<div id="loading" style="" ></div>');
        var action = 'fetch_data';
        var user_data = get_filter('profiles');
        
        $.ajax({
            url:"fetch_best_match.php",
            method:"POST",
            data:{action:action, user_data:user_data},
            success:function(data){
                $('.profiles-connect').html(data);
            }
        });

        $.ajax({
            url:"fetch_bestMatch_map.php",
            method:"POST",
            data:{action:action, user_data:user_data},
            success:function(data){
                $('#mapData').html(data);
            }
        });
    }

    function get_filter(class_name)
    {
        var filter="";
        $('.'+class_name+':selected').each(function(){
            filter=$(this).val();
        });
        return filter;
    }

    $('.profiles').click(function(){
        filter_data();
    });


});
</script>

    

