<?php
    session_start();
    if(isset($_SESSION["email"])){
        include 'headerLogged.php';
    }else{
        header('Location: login.php');
    }

    require_once 'database/dbConfig.php';

// On Accepting the connection request
if(isset($_POST["accept"])){
    $updateQuery = "UPDATE connections SET connection_status='Accepted' WHERE id='".$_POST['connId']."'";
    $Conn->query($updateQuery);
    if ($Conn->query($updateQuery) === TRUE) {
        echo "<script>alert('Accepted')</script>";
    }else{
        echo "<script>alert('Some Problem Occured')</script>";
    }
}
?>

<section class=" equal" style="">
    <section class="equal">
        <div class="container">
            <div class="row">
                <div class="">
<?php
            
//To Get All Connections & Connection Request

            //  If connection is established from other end
            $connArray=array();

            $profileQuery = "SELECT * FROM connections WHERE connection_email='".$_SESSION["email"]."'";
            $result = $Conn->query($profileQuery);
                                            
            if ($result->num_rows > 0){
                while($rows = $result->fetch_assoc()){
                    array_push($connArray,$rows["id"]);
                    if($rows["connection_status"]=="Sent"){
                        echo'
                        <form method="post">
                            <input type="hidden" name="connId" value="'.$rows["id"].'" />
                            <div class="row alert alert-dark">
                                <div class="col-md-8">
                                    <i class="fa fa-envelope"></i> <a href="view_profile.php?user='.$rows["user_id"].'">'.$rows["user_name"].'</a> wants to connect with '.$rows["connection_name"].'
                                </div>
                                <div class="col-md-4">
                                    <input type="submit" class="btn btn-warning" name="accept" value="Accept" />
                                </div>
                            </div>
                        </form>';
                    }else{
                        echo'
                        <div class="row alert alert-dark">
                                <div class="col-md-8">
                                    <i class="fa fa-envelope"></i> '.$rows["user_name"].' and <a href="view_profile.php?user='.$rows["connection_id"].'"> '.$rows["connection_name"].'</a> are connected.
                                </div>
                                <div class="col-md-4">
                                    '.$rows["connection_time"].'
                                </div>
                            </div>';
                    }
                    
                }
            }

            //  If connection is established from our end
            $query = "SELECT * FROM connections WHERE user_email='".$_SESSION["email"]."' && id NOT IN('" . implode( "', '" , $connArray ) . "' )";
            $results = $Conn->query($query);
                                            
            if ($results->num_rows > 0){
                while($row = $results->fetch_assoc()){
                    if($row["connection_status"]=="Sent"){
                        echo'
                        <form method="post">
                            <input type="hidden" name="connId" value="'.$row["id"].'" />
                            <div class="row alert alert-dark">
                                <div class="col-md-8">
                                    <i class="fa fa-envelope"></i> '.$row["user_name"].' have invited <a href="view_profile.php?user='.$row["connection_id"].'"> '.$row["connection_name"].'</a>
                                </div>
                                <div class="col-md-4">
                                    '.$row["connection_time"].'
                                </div>
                            </div>
                        </form>';
                    }else{
                        echo'
                        <div class="row alert alert-dark">
                                <div class="col-md-8">
                                    <i class="fa fa-envelope"></i> '.$row["user_name"].' and <a href="view_profile.php?user='.$row["connection_id"].'"> '.$row["connection_name"].'</a> are connected.
                                </div>
                                <div class="col-md-4">
                                    '.$row["connection_time"].'
                                </div>
                            </div>';
                    }
                    
                }
            }
?>
                </div>
            </div>
        </div>
    </section>
</section>
<?php include 'footer.php';?>