<?php
session_start();
if(!isset($_GET["userId"])){
   header('Location: profiles.php');
}
if(isset($_POST["addProfile"])){
    if ($_SERVER['REQUEST_METHOD'] == 'POST'){
        $languages=implode(', ',$_POST["languages"]);
        $interests=implode(', ',$_POST["interests"]);
        if(isset($_POST["us_stay_from"])){
            $us_stay_from=$_POST["us_stay_from"];
            $us_stay_to=$_POST["us_stay_to"];
            $us_stay_always=0;
        }else{
            $us_stay_from="";
            $us_stay_to="";
            $us_stay_always=1;
        }
        $target_dir = "Img/uploads/";
        if(!empty($_FILES["proPixUpload"])){
            if(isset($_FILES["proPixUpload"])){
                $target_file = $target_dir . basename($_FILES["proPixUpload"]["name"]);
                $uploadOk = 1;
                $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
                    $imageFileType = strtolower($imageFileType);
                    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                    && $imageFileType != "gif") {
                        echo "<script>alert('Only jpg, jpeg & png files are allowed')</script>";
                        $uploadOk = 0;
                    } else{
                        if ($uploadOk == 0) {
                            echo "<script>alert('Sorry, Your file was not uploaded')</script>";
                        } else {
                            $test = explode('.', $_FILES["proPixUpload"]["name"]);
                            $ext = end($test);
                            $saveas = rand(100, 999) . '.' . $ext;
                            
                                if (move_uploaded_file($_FILES["proPixUpload"]["tmp_name"], $saveas)) {
                                    require_once 'database/dbConfig.php';
                                    $updateQuery = "UPDATE members SET image='$saveas', title='".$_POST["title"]."', name='".$_POST["name"]."', age='".$_POST["age"]."', profileType='".$_POST["profileType"]."', languages='".$languages."', interests='".$interests."', country='".$_POST['country']."', state='".$_POST["state"]."', city='".$_POST["city"]."', us_address1='".$_POST["us_address1"]."', us_address2='".$_POST["us_address2"]."', us_zip='".$_POST["us_zip"]."', us_city='".$_POST["us_city"]."', us_stay_from='".$us_stay_from."', us_stay_to='".$us_stay_to."', us_stay_always='".$us_stay_always."' WHERE id='".$_GET['id']."'";
                                    $Conn->query($updateQuery);
                                    if ($Conn->query($updateQuery) === TRUE) {
                                        echo "<script>alert('Added Successfully')</script>";  
                                        header('location:profiles.php'); 
                                    }else{
                                        echo "<script>alert('Some Problem Occured')</script>";
                                    }
                                    
                            
                                    $Conn->close();
                                    
                                    
                                } else {
                                    echo "<script>alert('Sorry, there was an error uploading your file')</script>";
                                }
                        }
            
                    }
            
                
            }
        }else{
            require_once 'database/dbConfig.php';
            $updateQuery = "UPDATE members SET title='".$_POST["title"]."', name='".$_POST["name"]."', age='".$_POST["age"]."', profileType='".$_POST["profileType"]."', languages='".$languages."', interests='".$interests."', country='".$_POST['country']."', state='".$_POST["state"]."', city='".$_POST["city"]."', us_address1='".$_POST["us_address1"]."', us_address2='".$_POST["us_address2"]."', us_zip='".$_POST["us_zip"]."', us_city='".$_POST["us_city"]."', us_state='".$_POST["us_state"]."', us_stay_from='".$us_stay_from."', us_stay_to='".$us_stay_to."', us_stay_always='".$us_stay_always."' WHERE id='".$_GET['id']."'";
            $Conn->query($updateQuery);
            if ($Conn->query($updateQuery) === TRUE) {
                echo "<script>alert('Updated Successfully')</script>";  
                header('Location: profiles.php');
            }else{
                echo "<script>alert('Some Problem Occured')</script>";
            }
            
    
            $Conn->close();
        }
        
        
    } 
}else{
require_once 'database/dbConfig.php';
            $profileQuery = "SELECT * FROM members WHERE id='".$_GET['userId']."'";
            $result = $Conn->query($profileQuery);
                                            
            if ($result->num_rows > 0){
                $rows = $result->fetch_assoc();
                $languages=explode(', ',$rows["languages"]);
                $interests=explode(', ',$rows["interests"]);
                $age=$rows["age"];
                if(!empty($rows["us_stay_from"])){
                    $us_stay_from=$rows["us_stay_from"];
                    $us_stay_to=$rows["us_stay_to"];
                    
                }else{
                    $us_stay_from="";
                    $us_stay_to="";
                }
            include 'headerLogged.php';
            echo'
            <section class=" equal" style="">
                <section class="equal">
                        <div class="container registeration">
                                <h4><strong>Add Profile</strong></h4>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group"> 

                                <form class="form" method="post" id="registrationForm">
                            
                                    
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                Profile Type*
                                            </div>
                                        </div>
                                
                                        <div class="col-md-9">
                                            <div class="form-group">                         
                                                <select name="profileType" id="profileType" class="form-control" required>
                                                    <option value="">-</option>
                                                    <option value="self" ';if($rows["profileType"]=="self"){echo 'selected="selected"';} echo'>Self/Spouse</option>
                                                    <option value="parents" ';if($rows["profileType"]=="parents"){echo 'selected="selected"';} echo'>Parent</option>
                                                    <option value="inlaws" ';if($rows["profileType"]=="inlaws"){echo 'selected="selected"';} echo'>In-Laws</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                Name*
                                            </div>
                                        </div>
                                
                                        <div class="col-md-3">
                                            <div class="form-group">                         
                                                <select name="title" id="title" class="form-control" required>
                                                    <option value="">--</option>
                                                    <option value="Ms." ';if($rows["title"]=="Ms."){echo 'selected="selected"';} echo'>Ms.</option>
                                                    <option value="Mr." ';if($rows["title"]=="Mr."){echo 'selected="selected"';} echo'>Mr.</option>
                                                    <option value="Mrs." ';if($rows["title"]=="Mrs."){echo 'selected="selected"';} echo'>Mrs.</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">                         
                                                <input type="text" id="name" class="form-control" required name="name" value="'.$rows["name"].'">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                Age*
                                            </div>
                                        </div>
                                
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <select id="age" name="age" class="form-control">
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                Home Town*
                                            </div>
                                        </div>
                                
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input type="text" id="country" class="form-control" required name="country" value="'.$rows["country"].'" placeholder="Country*">
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input type="text" id="state" class="form-control" required name="state" value="'.$rows["state"].'" placeholder="State/Province*">
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input type="text" id="city" class="form-control" required name="city" value="'.$rows["city"].'" placeholder="City*">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                Upload Picture
                                            </div>
                                        </div>
                                
                                        <div class="col-md-9">
                                            <div id="uploadImage" class="form-group">';
                                                if($rows["image"]){
                                                    echo'<img src="Img/uploads/'.$rows["image"].'" style="height:20px;width:20px;" />
                                                        <button class"btn btn-dark remove" onclick="removeImage()">Remove Image</button>';
                                                }else{
                                                    echo'<input type="file" class="btn btn-dark form-control" style="width:60%;margin-bottom:10px;" name="proPixUpload" />';
                                                }
                                                echo'
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                            Select Languauge(s)*
                                            </div>
                                        </div>
                                
                                        <div class="col-md-9">
                                            <div class="form-group">
                                                <div class="row languages">
                                                    <div class="col-md-4">
                                                        <input type="checkbox" name="languages[]" ';if (in_array("Hindi", $languages)) {
                                                            echo "checked";
                                                        }
                                                        echo ' required value="Hindi" />Hindi
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="checkbox" name="languages[]" ';if (in_array("Telugu", $languages)) {
                                                            echo "checked";
                                                        }
                                                        echo ' value="Telugu" />Telugu
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="checkbox" name="languages[]" ';if (in_array("Marathi", $languages)) {
                                                            echo "checked";
                                                        }
                                                        echo ' value="Marathi" />Marathi
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="checkbox" name="languages[]" ';if (in_array("English", $languages)) {
                                                            echo "checked";
                                                        }
                                                        echo ' value="English" />English
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="checkbox" name="languages[]" ';if (in_array("Tamil", $languages)) {
                                                            echo "checked";
                                                        }
                                                        echo ' value="Tamil" />Tamil
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="checkbox" name="languages[]" ';if (in_array("Gujrati", $languages)) {
                                                            echo "checked";
                                                        }
                                                        echo ' value="Gujarati" />Gujarati
                                                    </div>
                                                </div>
                                                <div class="input-group mb-3">
                                                    <input type="text" class="form-control" id="lang1" placeholder="Add Languages">
                                                    <div class="input-group-append">
                                                        <input type="button" class="btn btn-dark addLang1" value="Add"> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                            Select Languauge(s)*
                                            </div>
                                        </div>
                                
                                        <div class="col-md-9">
                                            <div class="form-group">
                                                <div class="row interests">
                                                    <div class="col-md-4">
                                                        <input type="checkbox" name="interests[]" ';if (in_array("Walking", $interests)) {
                                                            echo "checked";
                                                        }
                                                        echo ' required value="Walking" />Walking
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="checkbox" name="interests[]" ';if (in_array("Socilaizing", $interests)) {
                                                            echo "checked";
                                                        }
                                                        echo ' value="Socializing" />Socializing
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="checkbox" name="interests[]" ';if (in_array("Yoga", $interests)) {
                                                            echo "checked";
                                                        }
                                                        echo ' value="Yoga" />Yoga
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="checkbox" name="interests[]" ';if (in_array("Cooking", $interests)) {
                                                            echo "checked";
                                                        }
                                                        echo ' value="Cooking" />Cooking
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="checkbox" name="interests[]" ';if (in_array("Watching TV/Movies", $interests)) {
                                                            echo "checked";
                                                        }
                                                        echo ' value="Watching TV/Movies" />Watching TV/Movies
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="checkbox" name="interests[]" ';if (in_array("Arts & Crafts", $interests)) {
                                                            echo "checked";
                                                        }
                                                        echo ' value="Arts & Crafts" />Arts & Crafts
                                                    </div>
                                                    
                                                </div>
                                                <div class="input-group mb-3">
                                                    <input type="text" class="form-control" id="int1" placeholder="Add Interests">
                                                    <div class="input-group-append">
                                                        <input type="button" class="btn btn-dark addInt1" value="Add"> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <h5>US Address</h5>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                            Address Line 1*
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="form-group">
                                                <input type="text" id="us_address1" class="form-control" required name="us_address1" value="'.$rows["us_address1"].'">
                                            </div>
                                        </div>
                                    </div>    

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                            Address Line 2*
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="form-group">
                                                <input type="text" id="us_address2" class="form-control" required name="us_address2" value="'.$rows["us_address2"].'">
                                            </div>
                                        </div>
                                    </div>    

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                            Zip Code*
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="form-group">
                                            <input type="text" id="us_zip" class="form-control" required name="us_zip" value="'.$rows["us_zip"].'">
                                            </div>
                                        </div>
                                    </div>     

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                            City*
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="form-group">
                                            <input type="text" id="us_city" class="form-control" required name="us_city" value="'.$rows["us_city"].'">
                                            </div>
                                        </div>
                                    </div>           
                                    
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                            State*
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="form-group">
                                            <input type="text" id="us_state" class="form-control" required name="us_state" value="'.$rows["us_state"].'">
                                            </div>
                                        </div>
                                    </div>           
                                    
                                    
                                    Keep This Profile Active<br>

                                    <input type="radio" name="myRadios1" onchange="stayChange1()" required  ';if(!empty($rows["us_stay_from"])){echo 'checked';} echo' /> During Stay in US &nbsp;
                                    <input type="radio" name="myRadios1" onchange="stayChange2()" ';if(empty($rows["us_stay_from"])){echo 'checked';} echo' /> Always
                                    <div id="stay1"></div>
                                    <input type="submit" name="addProfile" class="add btn btn-warning" value="Submit" />
                                    <a class="add btn btn-dark" href="profiles.php">Cancel</a>
                                </form>
                            </div>
                            </div>
                        </div>
                    </div>
                </section>
            </section>';
        }
    }
 include 'footer.php';
 ?>
 <script>
    for(i=18; i<100; i++){
        $("#age").append("<option value="+i+" `<?php if($age==`""+i+""`){echo'selected';}?>` >" + i + "</option>");
    }

//For Adding Languages

        var j=1;
        $('.addLang1').click(function(){
            j++;
            $('.languages').append(`<div class="col-md-4" id="lang1`+j+`">
                                <input id="`+j+`" class="lang1_remove" type="checkbox" name="languages[]" checked value="`+$('#lang1').val()+`" />`+$('#lang1').val()+`
                            </div>`);
                            $('#lang1').val("");
            
        });
        
        $(document).on('change', '.lang1_remove', function(){
            var lang1_id = $(this).attr("id"); 
            if(!$(this).is(':checked')){
                $("#lang1"+lang1_id+"").remove();
            }
           
        });
        
        
//For Adding Interests

        var l=1;
        $('.addInt1').click(function(){
            l++;
            $('.interests').append(`<div class="col-md-4" id="int1`+l+`">
                                <input id="`+l+`" class="int1_remove" type="checkbox" name="interests[]" checked value="`+$('#int1').val()+`" />`+$('#int1').val()+`
                            </div>`);
                            $('#int1').val("");
            
        });
        
        $(document).on('change', '.int1_remove', function(){
            var int1_id = $(this).attr("id"); 
            if(!$(this).is(':checked')){
                $("#int1"+int1_id+"").remove();
            }
           
        });

// On selecting the stay as 'During Stay in US'
function stayChange1(){ 
      $('#stay1').html(`
      <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                US Arrival Date*
            </div>
        </div>
        <div class="col-md-9">
            <div class="form-group">
                <input type="text" id="from1" name="us_stay_from" class="form-control" required placeholder="From" value="<?php echo $us_stay_from;?>">
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
                US Departure Date*
            </div>
        </div>
        <div class="col-md-9">
            <div class="form-group">
                <input type="text" id="to1" name="us_stay_to" class="form-control" required placeholder="To" value="<?php echo $us_stay_from;?>">
            </div>
        </div>      
    </div>      
    `);
    }

// On selecting the stay as 'Always'
    function stayChange2(){ 
      $('#stay1').html("");
    }  

//For removing profile pic
function removeImage(){
  $('#uploadImage').html(`<input type="file" class="btn btn-dark form-control" style="width:60%;margin-bottom:10px;" name="proPixUpload" />`);
}      
 </script>