<?php
   session_start();
   require_once 'database/dbConfig.php';

   if(isset($_POST["action"]))
    {   
        // Recieved user Data on change of selection
        $user_data=explode(',',$_POST["user_data"]);
        $user_id=$user_data[0];
        $user_name=$user_data[1];
        $user_lat=$user_data[2];
        $user_long=$user_data[3];

        $_SESSION["primary_id"]=$user_id;
        $_SESSION["primary_name"]=$user_name;

        // To get list of profiles already connected or requested for connection
        $connArray=array();
        $connections="SELECT * FROM connections WHERE user_id='".$user_id."' OR connection_id='".$user_id."'";
        $res = $Conn->query($connections);                              
            if ($res->num_rows > 0){
                while($row = $res->fetch_assoc()){
                    array_push($connArray,$row["connection_id"]);
                    array_push($connArray,$row["user_id"]);
                }
            }

        // To Get All Best Match Profiles: in the area of 5 KM and not in connections list
        $searchQuery = "SELECT * , ( 6371 * ACOS( COS( RADIANS(  '".$user_lat."' ) ) * COS( RADIANS(  `lat` ) ) * COS( RADIANS(  `long` ) - RADIANS(  '".$user_long."' ) ) + SIN( RADIANS(  '".$user_lat."' ) ) * SIN( RADIANS(  `lat` ) ) ) ) AS distance
                        FROM members
                        HAVING distance <  '5'
                        && email!='".$_SESSION["email"]."' && id NOT IN('" . implode( "', '" , $connArray ) . "' )
                        ORDER BY distance";
          

            $result = $Conn->query($searchQuery);
            echo'<span> '.$result->num_rows.' Profile(s) found in distance of 5 KM</span>';
            if ($result->num_rows > 0){
                while($rows = $result->fetch_assoc()){
                echo'
                <ul id="bestMatchData">
              
                <li>			
                    <div class="prof-wid150">';
                    if(empty($rows["image"])){
                        echo'<img src="Img/boy-small.png" alt="'.$rows["name"].'" title="'.$rows["name"].'" id="profile_icon"/>';
                    }else{
                        echo'<img src="Img/uploads/'.$rows["image"].'" alt="'.$rows["name"].'" title="'.$rows["name"].'" id="profile_icon"/>';
                    }
                    echo'    
                        
                    </div>
                    

                    <div class="prof-wid372 inner-prof-connect">
                        <div class="col-md-12">
                            <a href="javascript:void(0);"><h4>'.$rows["title"].' '.$rows["name"].'<span> ('.$rows["age"].' years)</span></h4></a>
                            <br/>
                            <span><h5 class="">'.$rows["us_city"].' '.$rows["us_state"].' '.$rows["us_zip"].'<br/></h5></span>
                        </div>
                        <div class="col-md-6 width50">
                            <h5 class="profile-street-home"> 
                            '.$rows["city"].'
                            </h5>                                                                  
                            <h5 class="profile-street profile-street-first-more">'.$rows["interests"].'</h5>                                                                                                                                
                        </div>
                        <div class="col-md-6 width50 padding10">
                            <h5 class="profile-speaks profile-street-first-more">'.$rows["languages"].'</h5>                                                              
                        </div>    

                    </div>
                    <div class="prof-wid182">
                        <form method="post">
                            <input type="hidden" name="user_id" value="'.$user_id.'" />
                            <input type="hidden" name="user_name" value="'.$user_name.'" />
                            <input type="hidden" name="connection_id" value="'.$rows["id"].'" />
                            <input type="hidden" name="connection_name" value="'.$rows["title"].' '.$rows["name"].'" />
                            <input type="hidden" name="connection_email" value="'.$rows["email"].'" />  
                            <button type="submit" name="connect" class="btn btn-default" title="CONNECT">CONNECT</button> 
                        </form>
                    </div>
                </li>			
            </ul>
                    ';
                }
            }
                
           
    }

	
?>